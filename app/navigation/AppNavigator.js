import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {SplashScreen, HomeScreen} from '@app/screens';

const Stack = createNativeStackNavigator();

import React from 'react';

export default function AppNavigator() {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen
        name="Splash"
        component={SplashScreen}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
}
