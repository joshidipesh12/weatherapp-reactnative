import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Image,
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  FlatList,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {Colors} from '@app/theme';
import {Icon} from 'react-native-elements';
import {Screen} from '@app/components';

const forecast = [
  {
    key: 1,
    day: 'Wed 16',
    icon: 'https://img.icons8.com/fluency/2x/partly-cloudy-day.png',
  },
  {
    key: 2,
    day: 'Thu 17',
    icon: 'https://img.icons8.com/fluency/2x/partly-cloudy-rain.png',
  },
  {
    key: 3,
    day: 'Fri 18',
    icon: 'https://img.icons8.com/fluency/2x/chance-of-storm.png',
  },
  {
    key: 4,
    day: 'Sat 19',
    icon: 'https://img.icons8.com/fluency/2x/cloud-lighting.png',
  },
  {
    key: 5,
    day: 'Sun 20',
    icon: 'https://img.icons8.com/fluency/2x/partly-cloudy-day.png',
  },
  {
    key: 6,
    day: 'Mon 21',
    icon: 'https://img.icons8.com/fluency/2x/stormy-night.png',
  },
  {
    key: 7,
    day: 'Tue 22',
    icon: 'https://img.icons8.com/fluency/2x/fog-night.png',
  },
];

const HomeScreen = ({}) => {
  const color = Colors.LIGHT_TEXT;

  const renderForecastItem = ({item}) => (
    <View
      style={{
        height: 70,
        margin: 20,
        alignItems: 'center',
        justifyContent: 'space-between',
      }}>
      <Text style={{color: 'white'}}>{item.day}</Text>
      <Image
        source={{uri: item.icon}}
        style={{height: wp('12%'), width: wp('12%')}}
      />
    </View>
  );

  return (
    <Screen style={styles.container}>
      <View style={styles.topLocation}>
        <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center'}}>
          <Icon
            name="map-marker"
            type="material-community"
            size={26}
            color={color}
          />
          <Text style={{fontSize: 20, color, marginHorizontal: 5}}>
            New York
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={{transform: [{rotateY: '180deg'}]}}>
          <Icon
            name="magnify"
            type="material-community"
            size={26}
            color={color}
          />
        </TouchableOpacity>
      </View>
      <View style={styles.mainContent}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{width: '100%', alignItems: 'center'}}>
            <Text
              style={{
                color,
                fontSize: 24,
                fontWeight: 'bold',
                marginTop: 10,
              }}>
              Wed, 16 October
            </Text>
            <Text style={{color, fontSize: 14, fontFamily: 'monospace'}}>
              Updated as of 10:14 PM GMT+5:30
            </Text>
            <Image
              source={require('../assets/weather/WindyNight.png')}
              style={{height: 260, width: 260, marginTop: -40}}
            />
            <View style={{flexDirection: 'row', marginTop: -15}}>
              <Text style={{color, fontSize: 86, marginTop: -10}}>25</Text>
              <Text style={{color, fontSize: 24, fontWeight: 'bold'}}>
                {'\u00B0'}C
              </Text>
            </View>
            <Text
              style={{
                color,
                fontSize: 36,
                fontWeight: 'bold',
                fontFamily: 'monospace',
              }}>
              Windy
            </Text>
            <View style={{flexDirection: 'row', marginTop: 20}}>
              <View style={styles.otherParams}>
                <Icon
                  name="water-outline"
                  type="material-community"
                  size={40}
                  color={color}
                />
                <Text style={{color}}>HUMIDITY</Text>
                <Text style={{color}}>52%</Text>
              </View>
              <View style={styles.otherParams}>
                <Icon
                  name="weather-windy"
                  type="material-community"
                  size={40}
                  color={color}
                />
                <Text style={{color}}>WIND</Text>
                <Text style={{color}}>52%</Text>
              </View>
              <View style={styles.otherParams}>
                <Icon
                  name="thermometer"
                  type="material-community"
                  size={40}
                  color={color}
                />
                <Text style={{color}}>FEELS LIKE</Text>
                <Text style={{color}}>52%</Text>
              </View>
            </View>
            <View style={styles.bottomWeather}>
              <FlatList
                horizontal={true}
                data={forecast}
                renderItem={renderForecastItem}
                keyExtractor={item => item.key}
                showsHorizontalScrollIndicator={false}
                style={{paddingHorizontal: 10}}
              />
            </View>
          </View>
        </ScrollView>
      </View>
    </Screen>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  topLocation: {
    width: wp('90%'),
    paddingVertical: 10,
    marginTop: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  mainContent: {
    borderRadius: 24,
    width: wp('96%'),
    height: hp('90%'),
    alignSelf: 'center',
    overflow: 'hidden',
  },
  otherParams: {
    flex: 1,
    height: 100,
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  bottomWeather: {
    width: '100%',
    backgroundColor: 'rgba(83, 83, 83, 0.6)',
    marginTop: 20,
    borderRadius: 24,
    overflow: 'hidden',
    height: 300,
  },
});

export default HomeScreen;
