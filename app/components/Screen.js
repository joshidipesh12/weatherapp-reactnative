import React from 'react';
import {ImageBackground, Platform, View, StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

const ScreenView = ({children, style}) => {
  return (
    <View style={[styles.screen, style]}>
      <ImageBackground
        source={require(`@app/assets/Background.png`)}
        style={styles.background}
        blurRadius={20}>
        {children}
      </ImageBackground>
    </View>
  );
};

export default ScreenView;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    paddingTop: Platform.OS == 'ios' ? 20 : 0,
    backgroundColor: 'grey',
  },
  background: {
    height: '100%',
    width: wp('100%'),
  },
});
