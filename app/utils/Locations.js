import GetLocation from 'react-native-get-location';

const get = () => {
  return GetLocation.getCurrentPosition({
    enableHighAccuracy: false,
    timeout: 15000,
  });
};
export default {get};
