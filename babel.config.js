module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: ['syntax-dynamic-import', '@babel/plugin-syntax-dynamic-import'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['.'],
        extensions: [
          '.ios.ts',
          '.android.ts',
          '.ts',
          '.ios.tsx',
          '.android.tsx',
          '.tsx',
          '.jsx',
          '.js',
          '.json',
        ],
        alias: {
          '@app/assets': './app/assets',
          '@app/components': './app/components',
          '@app/constants': './app/constants',
          '@app/navigation': './app/navigation',
          '@app/screens': './app/screens',
          '@app/store': './app/store',
          '@app/theme': './app/theme',
          '@app/utils': './app/utils',
        },
      },
    ],
  ],
};
